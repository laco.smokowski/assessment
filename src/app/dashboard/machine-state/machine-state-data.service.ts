import { Injectable } from '@angular/core';
import { RestService } from 'src/app/shared/rest.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MachineStateDataService {
  machineStateSubject = new Subject();

  constructor(private restService: RestService) { }

  getMachineState(): any {
    this.restService.getData("https://www.marviq.com/assessment/index.php?page=a&from=2018-01-07%2000:00:00")
    .subscribe((data) => {
      this.machineStateSubject.next({data: data, dataType: 'machineState'});
    });
  }

  getMachineTemperature() {
    this.restService.getData("https://www.marviq.com/assessment/index.php?page=b&from=2018-01-07%2000:00:00")
    .subscribe(data => {
      this.machineStateSubject.next({data: data, dataType: 'machineTemperature'});
    })
  }
}
