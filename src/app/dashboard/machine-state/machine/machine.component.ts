import { Component, OnInit, Input } from '@angular/core';
import { ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss']
})
export class MachineComponent implements OnInit {
  @Input() machine;
  chartType: ChartType = 'line';
  labels: Label[] = [];
  values: ChartDataSets[] = [{data: [], label: ''}];
  legend = false;
  

  constructor() { }

  ngOnInit(): void {
    this.getDataForChart();
  }

  getDataForChart() {
    Object.keys(this.machine).forEach(key => {
      if (key.match('^H[0-23]')) {
        this.values[0].data.push(this.machine[key]);
        this.labels.push(key)
      } 
    });
  }

}
