import { Component, OnInit, OnDestroy } from '@angular/core';
import { MachineStateDataService } from './machine-state-data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-machine-state',
  templateUrl: './machine-state.component.html',
  styleUrls: ['./machine-state.component.scss']
})
export class MachineStateComponent implements OnInit, OnDestroy {
  data: any[] = [];
  dataLoaded = false;
  temperatureLoaded = false;
  machineStateSubsc: Subscription

  constructor(private machineStateService: MachineStateDataService) { }

  ngOnInit(): void {
    this.machineStateService.getMachineState();
    
    this.machineStateSubsc = this.machineStateService.machineStateSubject
    .subscribe((subscData: {data: any, dataType: string}) => {
      if (subscData.dataType === 'machineState') {
        this.data = subscData.data;
        this.dataLoaded = true;
        this.machineStateService.getMachineTemperature();
      } else if (subscData.dataType === 'machineTemperature') {
        this.data.forEach(machine => {
          machine['TEMPERATURE'] = subscData.data[machine.MACHINE].split('/');
          this.temperatureLoaded = true;
        });
      }
    });

    
  }

  ngOnDestroy() {
    this.machineStateSubsc.unsubscribe();
  }

}
